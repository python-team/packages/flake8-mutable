Source: flake8-mutable
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Edward Betts <edward@4angle.com>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-pytest <!nocheck>,
               python3-pytest-runner <!nocheck>,
               python3-setuptools
Rules-Requires-Root: no
Standards-Version: 4.6.1
Homepage: https://github.com/ebeweber/flake8-mutable
Vcs-Browser: https://salsa.debian.org/python-team/packages/flake8-mutable
Vcs-Git: https://salsa.debian.org/python-team/packages/flake8-mutable.git

Package: python3-flake8-mutable
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: use flake8 to catch functions with mutable default arguments
 Python's default arguments are evaluated at definition as opposed to when the
 function is invoked. This leads to unexpected behavior, as mutations persist
 between calls. This flake8 extension will warn for this type of mistake.
